import domain.Task;
import domain.TaskList;
import exceptions.SaveTaskListToFileException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import persistence.FileManagement;
import persistence.FileManagementImplementation;

public class SaveLoadTasklistTests {
    // Alle Methoden der File-Management-Klasse mit maximaler Code-Coverage sind zu testen

    private TaskList taskList;
    private Task task;
    private FileManagement fileManagement;

    @BeforeEach
    public void setUp() {
        Mockito.mock(TaskList.class);
        Mockito.mock(Task.class);
        this.fileManagement = new FileManagementImplementation();
    }

    @Test
    public void testSaveTaskListToFileNull() {
        Assertions.assertDoesNotThrow(() -> this.fileManagement.saveTaskListToFile(this.taskList));
    }

    @Test
    public void testSaveTaskListToFileNullThrow() {
        Assertions.assertThrows(SaveTaskListToFileException.class, () -> this.fileManagement.saveTaskListToFile(this.taskList));
    }
}

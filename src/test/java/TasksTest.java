import domain.*;
import exceptions.DeadlineNotValidException;
import exceptions.TaskDescriptionNotValidException;
import exceptions.TaskPriorityNotValidException;
import exceptions.TaskTitleNotValidException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDateTime;
import java.util.Set;

public class TasksTest {
    // Alle Methoden der Taskklasse sind mit maximaler Code-Coverage zu testen
    // Es sind an passenden Stellen parametrisierte Tests zu verwenden

    private TaskTitle taskTitle;
    private TaskDescription taskDescription;
    private TaskStatus taskStatus;
    private LocalDateTime creationTimestamp;
    private LocalDateTime deadline;
    private TaskPriority taskPriority;
    private Set<Tag> tagList;
    private boolean pinned;
    private Task task;
    private int prio;

    @BeforeEach
    public void setup() {
        this.prio = 8;
        this.taskTitle = new TaskTitle("Title");
        this.taskDescription = new TaskDescription("Description");
        this.taskStatus = TaskStatus.open;
        this.creationTimestamp = LocalDateTime.of(2022, 4, 6, 9, 0);
        this.deadline = LocalDateTime.of(2022, 4, 6, 12, 30);
        this.taskPriority = new TaskPriority(this.prio);
        this.pinned = true;
        this.task = new Task(this.taskTitle, this.taskDescription, this.deadline, this.taskPriority);
        this.task.addTag(new Tag("tag"));
    }


    @Test
    public void testKonstruktorAnlegenRichtig() {
        /*     Assertions.assertThrows(TaskTitleNotValidException.class, () -> new Task(new TaskTitle(null), this.taskDescription, this.deadline, this.taskPriority));*/
        Task task = new Task(this.taskTitle, this.taskDescription, this.deadline, this.taskPriority);
        Assertions.assertEquals(this.taskTitle.toString(), task.getTaskTitle().toString());
        Assertions.assertEquals(this.taskDescription.toString(), task.getTaskDescription().toString());
        Assertions.assertEquals(this.deadline.toString(), task.getDeadline().toString());
        Assertions.assertEquals(this.taskPriority, task.getTaskPriority());
    }

    @Test
    public void testKonstruktorAnlegenMitThrowsUndNull() {
        Assertions.assertThrows(TaskPriorityNotValidException.class, () -> new Task(this.taskTitle, this.taskDescription, this.deadline, new TaskPriority(90)));
        Assertions.assertThrows(TaskTitleNotValidException.class, () -> new Task(null, this.taskDescription, this.deadline, this.taskPriority));
        Assertions.assertThrows(TaskTitleNotValidException.class, () -> new Task(new TaskTitle(""), this.taskDescription, this.deadline, this.taskPriority));
        Assertions.assertThrows(TaskDescriptionNotValidException.class, () -> new Task(this.taskTitle, new TaskDescription(null), this.deadline, this.taskPriority));
        Assertions.assertThrows(TaskDescriptionNotValidException.class, () -> new Task(this.taskTitle, new TaskDescription(""), this.deadline, this.taskPriority));
        Assertions.assertEquals(1, new Task(this.taskTitle, this.taskDescription, this.deadline, null).getTaskPriority().priority());
    }
/*

    @ParameterizedTest(name = "TaskTitle: {0}, TaskDescription: {1}, LocalDateTime: {2}, TaskPriority: {3}")
    @CsvSource({"Task1,Die ist ein Task,10.0", "Task2,Die ist ein Task,11.0", "Task3,Die ist ein Task,12.0"})
    @Test
    public void testKonstruktorAnlegenRichtigMitParameters(TaskTitle taskTitle, TaskDescription taskDescription, LocalDateTime deadline, TaskPriority taskPriority) {
            Assertions.assertThrows(TaskTitleNotValidException.class, () -> new Task(new TaskTitle(null), this.taskDescription, this.deadline, this.taskPriority));
        Task task = new Task(this.taskTitle, this.taskDescription, this.deadline, this.taskPriority);
        Assertions.assertEquals(this.taskTitle.toString(), task.getTaskTitle().toString());
        Assertions.assertEquals(this.taskDescription.toString(), task.getTaskDescription().toString());
        Assertions.assertEquals(this.deadline.toString(), task.getDeadline().toString());
        Assertions.assertEquals(this.taskPriority, task.getTaskPriority());
    }*/


    @Test
    public void testSetTaskTitleNull() {
        Assertions.assertThrows(TaskTitleNotValidException.class, () -> this.task.setTaskTitle(new TaskTitle(null)));
    }

    @Test
    public void testSetTaskTitleRichtig() {
        this.task.setTaskTitle(new TaskTitle("Task 2"));
        Assertions.assertEquals("Task 2", this.task.getTaskTitle().taskTitle());
    }

   /* @Test
    public void testSetTaskTitleRichtig() {
        Assertions.assertDoesNotThrow(TaskTitleNotValidException.class, () -> this.task.setTaskTitle(new TaskTitle("Test")));

    }*/


    @Test
    public void taskSetDescriptionNull() {
        Assertions.assertThrows(TaskDescriptionNotValidException.class, () -> this.task.setTaskDescription(new TaskDescription(null)));
    }

 /*   @Test
    public void taskSetDeadlineNull() {
        Assertions.assertThrows(DeadlineNotValidException.class, () -> this.task.setDeadline(new LocalDateTime(null)));
    }
*/

    @ParameterizedTest(name = "Alter Tagname: {0}, Neuer Tagname: {1}")
    @CsvSource({"tag,neuerTag"})
    @Test
    public void testChangeTagName(String oldName, String newName) {
        //  Assertions.assertEquals(true, this.task.getTagList().contains("tag"));
        this.task.changeTagName(new Tagname(oldName), new Tagname(newName));
    }

    @Test
    public void testSetTaskDescription() {
        this.task.setTaskDescription(new TaskDescription("Beschreibung"));
        Assertions.assertEquals("Beschreibung", this.task.getTaskDescription().taskdescription().toString());
    }

    @Test
    public void testSetTaskStatusTo() {
        this.task.setTaskStatusTo(TaskStatus.open);
        Assertions.assertEquals(TaskStatus.open, this.task.getTaskStatus());
    }

    @Test
    public void testSetTaskPriorityOhneThrow() {
        this.task.setTaskPriority(new TaskPriority(5));
        Assertions.assertEquals(5, this.task.getTaskPriority().priority());
    }

    @Test
    public void testSetTaskPriorityMitThrow() {
        Assertions.assertThrows(TaskPriorityNotValidException.class, () -> this.task.setTaskPriority(null));
        Assertions.assertEquals(this.prio, this.task.getTaskPriority().priority());
    }

    @Test
    public void testSetDeadlineOhneThrow() {
        LocalDateTime testDate = LocalDateTime.of(2022, 6, 4, 6, 9);
        this.task.setDeadline(testDate);
        Assertions.assertEquals(testDate, this.task.getDeadline());
    }

    @Test
    public void testSetDeadlineMitThrow() {
        Assertions.assertThrows(DeadlineNotValidException.class, () -> this.task.setDeadline(null));
        Assertions.assertEquals(LocalDateTime.of(2022, 4, 6, 12, 30), this.task.getDeadline());
    }

    @Test
    public void testIsTaskPinned() {
        this.task.pinTask();
        Assertions.assertTrue(this.task.isPinned());
    }

    @Test
    public void testIsTaskUnpinned() {
        this.task.unpinTask();
        Assertions.assertFalse(this.task.isPinned());
    }
}

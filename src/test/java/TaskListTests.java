import domain.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.Set;

public class TaskListTests {
    // Alle Methoden der Taskliste sind mit maximaler Code-Coverage zu testen
    // Es sind an passenden Stellen parametrisierte Tests zu verwenden
    // Die importData-Methode muss mit Mock getestet werden.

    private TaskList tasks;
    private Task task;
    private TaskTitle taskTitle;
    private TaskDescription taskDescription;
    private TaskStatus taskStatus;
    private LocalDateTime creationTimestamp;
    private LocalDateTime deadline;
    private TaskPriority taskPriority;
    private Set<Tag> tagList;
    private boolean pinned;

    @BeforeEach
    public void setup() {
        this.tasks = Mockito.mock(TaskList.class);
        this.taskTitle = new TaskTitle("Title");
        this.taskDescription = new TaskDescription("Description");
        this.taskStatus = TaskStatus.open;
        this.creationTimestamp = LocalDateTime.of(2022, 4, 6, 9, 0);
        this.deadline = LocalDateTime.of(2022, 4, 6, 12, 30);
        this.taskPriority = new TaskPriority(8);

        this.task = new Task(this.taskTitle, this.taskDescription, this.deadline, this.taskPriority);
    }

    @Test
    public void testGetTaskFromIndex() {
        Mockito.when(this.tasks.getTaskFromIndex(0)).thenReturn(this.task);
        Assertions.assertEquals(this.task.getDeadline(), this.tasks.getTaskFromIndex(0).getDeadline());
    }

    @Test
    public void testGetTaskFromIndexNull() {
        Mockito.when(this.tasks.getTaskFromIndex(0)).thenReturn(this.task);
        Assertions.assertEquals(null, this.tasks.getTaskFromIndex(9));
    }


}
